﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PassionProject.Models
{
    public class Game
    {
        //NEED GAMEID, NAME, RELEASE DATE, REVIEWS

        [Key, ScaffoldColumn(false)]
        public int GameId { get; set; }

        [Required, StringLength(255)]
        public string GameTitle { get; set; }

        [Required, Display(Name = "Release Date")]
        public int ReleaseDate { get; set; }

        [Required, StringLength(255), Display(Name = "Publisher")]
        public string Publisher { get; set; }

        [Required, StringLength(255), Display(Name="Category")]
        public virtual ICollection<GameReview> reviews { get; set; }
    }
}