﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PassionProject.Models
{
    public class GameReview
    {
        //gameReviewId, gameId, reviewName, Rating, RatingComment

        [Key, ScaffoldColumn(false)]
        public int Game_review_ID { get; set; }

        /*
         * this is the correct way of viewing the information (one game to many reviews)
        [Required, Display(Name = "Game ID")]
        public int gameId { get; set; }
        */
        //represent it like this in EntityFramework 6
        public virtual Game game { get; set; }

        [Required, Display(Name = "Content")]
        public string content { get; set; }

        [Required, Display(Name = "Review name")]
        public string reviewTitle { get; set; }

        [Required, Display(Name = "Rating")]
        public string rating { get; set; }

        [Required, StringLength(255), Display(Name = "Release Date")]
        public string ratingComment { get; set; }
    }
}