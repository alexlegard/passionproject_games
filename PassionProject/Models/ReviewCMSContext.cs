﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PassionProject.Models
{
    public class ReviewCMSContext : DbContext
    {
        public ReviewCMSContext()
        {

        }

        public DbSet<Game> Games { get; set; }
        public DbSet<GameReview> Reviews { get; set; }
    }
}