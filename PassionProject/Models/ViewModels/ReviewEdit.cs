﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionProject.Models.ViewModels
{
    public class ReviewEdit
    {
        //ReviewEdit is a class that represents an edit to a review.

        public ReviewEdit()
        {

        }

        //Represents the game that the review is assigned...
        public virtual Game game { get; set; }

        //Represents the gmes that it could be assigned to
        public IEnumerable<Game> games { get; set; }
    }
}