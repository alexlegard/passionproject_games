﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PassionProject.Models;
using PassionProject.Models.ViewModels;
using System.Diagnostics;

namespace PassionProject.Controllers
{
    public class GameController : Controller
    {
        //Variable to hold the db
        private ReviewCMSContext db = new ReviewCMSContext();

        //Have to create methods for create, read, update, and delete.
        //Create: New and Create
        //Read: Show
        //Update: Edit
        //Delete: Delete
        //
        //Current methods: Add, Create, 

        public ActionResult Add()
        {
            return View(db.Games);
        }

        //For the create: We write a string query with some of the
        //parameters in the query with the prefix @, then we execute
        //the db.Database.ExecuteSqlCommand passing four SqlParameters.
        //Here we use an array but we don't really need to as long as
        //we pass four SqlParameters...
        [HttpPost]
        public ActionResult Create(string gametitle,
            string releasedate, string publisher)
        {
            //My columns are in the query below...
            string query = "insert into games (" +
                "GameTitle, ReleaseDate, Publisher)" +
                "values ( @game_title, @release_date, " +
                "@publisher)";

            Debug.WriteLine(query);

            //SqlParameter[x] is an array of x SqlParameters.
            //SqlParameter("str", p) is an individual SqlParameter.
            SqlParameter[] gameparams = new SqlParameter[3];
            
            gameparams[0] = new SqlParameter("@game_title", gametitle);
            gameparams[1] = new SqlParameter("@release_date", releasedate);
            gameparams[2] = new SqlParameter("@publisher", publisher);

            db.Database.ExecuteSqlCommand(query, gameparams);

            return RedirectToAction("Manage");
        }

        //For Show (view), all we need to do is return a View. View has one parameter
        //which is a query which we call with db.Games.SqlQuery passing the @parameter
        //and parameter.

        public ActionResult Show(int id)
        {
            string query = "select * from games where gameid = @id";
            return View(db.Games.SqlQuery(query, new SqlParameter("@id", id)).FirstOrDefault());
        }

        public ActionResult Manage()
        {
            //data needed: all games in DB
            return View(db.Games.ToList());
        }
            
        public ActionResult Index()
        {
            return RedirectToAction("Manage");
        }
        public ActionResult Delete(int id)
        {
            //Create a query where @id is the id passed to the method
            string query = "delete from games where gameid = @id";
            
            //Execute the SQL query
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            //Here is where you would delete any references to this game in
            //the gamereviews table then execute the query.
            //query = "delete from gamereviews where gameid = @id";
            //db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            //Why doesn't it just call the list method?
            return RedirectToAction("Manage");
        }
        //For Edit, we create a new GameReviewEdit object 
        /*
        public ActionResult Edit(int? id)
        {
            //We edit by creating a pageedit object.
            PageEdit pageedit = new PageEdit();

            pageedit.game = db.Games.Find(id);
            pageedit.gamereview = db.gamereviews.ToList();

            if (pageedit.game != null)
            {
                return View(pageedit);
            } else
            {
                return HttpNotFound();
            }
        }
        */
        /*
        public ActionResult Edit(int? gameid, string gametitle, string releasedate, string publisher)
        {
            if ((gameid == null) || (db.Pages.Find(gameid) == null))
            {
                return HttpNotFound();
            }
            string query = "update games set gametitle = @title, " +
                "releasedate = @releasedate, publisher = @publisher " +
                "where gameid = @gameid";

            //Create an array of game parameters like before
            SqlParameter[] gameparams = new SqlParameter[3];

            gameparams[0] = new SqlParameter();
            gameparams[0].ParameterName = "@gametitle";
            gameparams[0].Value = gametitle;

            gameparams[1] = new SqlParameter();
            gameparams[1].ParameterName = "@releasedate";
            gameparams[1].Value = releasedate;

            gameparams[2] = new SqlParameter();
            gameparams[2].ParameterName = "@publisher";
            gameparams[2].Value = publisher;

            return RedirectToAction("Show/" + gameid);
        }
        */
    }
}