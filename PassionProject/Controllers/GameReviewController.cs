﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PassionProject.Models;
using System.Diagnostics;

namespace PassionProject.Controllers
{
    public class GameReviewController : Controller
    {
        private ReviewCMSContext db = new ReviewCMSContext();


        public ActionResult Manage()
        {
            return View(db.Reviews.ToList());
        }
        public ActionResult Add()
        {
            return View(db.Reviews.ToList());
        }
        public ActionResult Create(string reviewtitle, string content, string rating, string ratingcomment)
        {
            string query = "Insert into GameReviews (reviewTitle, content, rating, ratingComment)" +
                "Values (@reviewTitle, @content, @rating, @ratingComment)";

            SqlParameter[] gamereviewparams = new SqlParameter[4];

            gamereviewparams[0] = new SqlParameter("@reviewTitle", reviewtitle);
            gamereviewparams[1] = new SqlParameter("@content", content);
            gamereviewparams[2] = new SqlParameter("@rating", rating);
            gamereviewparams[3] = new SqlParameter("@ratingComment", ratingcomment);
            //gamereviewparams[4] = new SqlParameter("@game_GameId", gameId);

            db.Database.ExecuteSqlCommand(query, gamereviewparams);

            return RedirectToAction("Manage");
        }
        public ActionResult Delete(int id)
        {
            string query = "Delete from gamereviews where Game_review_ID = @id";

            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            return RedirectToAction("Manage");
        }
        public ActionResult Show(int id)
        {
            string query = "Select * from gamereviews where Game_review_ID = @id";
            return View(db.Games.SqlQuery(query, new SqlParameter("@id", id)).FirstOrDefault());
        }
        public ActionResult Edit(int id)
        {
            return View(db.Reviews.ToList());
        }
        //Gam
        
        public ActionResult Edit_it(string reviewtitle, string content, string rating, string ratingcomment)
        {
            string query = "update gamereviews set reviewTitle=@reviewtitle, " +
                "content=@content, rating=@rating, ratingComment=@ratingcomment";

            //Borrowing the idea of the array of parameters from the code example...
            SqlParameter[] reviewParams = new SqlParameter[4];

            reviewParams[0] = new SqlParameter();
            reviewParams[0].ParameterName = "@reviewTitle";
            reviewParams[0].Value = reviewtitle;

            reviewParams[1] = new SqlParameter();
            reviewParams[1].ParameterName = "@content";
            reviewParams[1].Value = content;

            reviewParams[2] = new SqlParameter();
            reviewParams[2].ParameterName = "@rating";
            reviewParams[2].Value = rating;

            reviewParams[3] = new SqlParameter();
            reviewParams[3].ParameterName = "@ratingComment";
            reviewParams[3].Value = ratingcomment;

            db.Database.ExecuteSqlCommand(query, reviewParams);
            return RedirectToAction("Manage");
        }
    }
}